// This file helps vapigen generate a good vapi for ostree

// Misc. fixes
* cheader_filename="ostree.h" // Include the right headerfile
AsyncProgress.new_and_connect skip // Unnecessary callback mess
ChecksumInputStream.checksum skip // Impl. detail not part of the API
Deployment.set_bootserial skip // According to docs should never be used
Sysroot // Remove ugly Sysroot.@default mess
  .new.path default=null
  .new_default skip
*_finish skip // Handled by async keyword
Repo.find_remotes_async.finders nullable // Missing nullable

// Fix missing arrays
KernelArgs
  .*_argv*.argv array array_null_terminated
  .append_argv_filtered.prefixes array array_null_terminated
checksum_inplace_to_bytes.buf array
cmp_checksum_bytes.*#parameter array

// Group related symbols
validate_* parent="OSTree.Validate" name="validate_(.*)"
COMMIT_META_KEY_* parent="OSTree.CommitMeta" name="COMMIT_META_(.*)"
METADATA_KEY_* parent="OSTree.CommitMeta" name="METADATA_(.*)"
commit_metadata_for_bootable parent="OSTree.CommitMeta" name="add_for_bootable"
*_GVARIANT_STRING parent="OSTree.GVariant"
metadata_variant_type parent="OSTree.GVariant"

// Capitalization fixes
OSTree name="OSTree"
SePolicy name="SEPolicy"
SePolicyRestoreconFlags name="SEPolicyRestoreconFlags"
GpgError name="GPGError"
GpgSignatureAttr name="GPGSignatureAttr"
GpgSignatureFormatFlags name="GPGSignatureFormatFlags"
GpgVerifyResult name="GPGVerifyResult"

// Enum name fixes
RepoCommitTraverseFlags.repo_commit_traverse_flag_none name="NONE"
GpgSignatureFormatFlags.gpg_signature_format_default name="DEFAULT"
StaticDeltaIndexFlags.static_delta_index_flags_none name="NONE"

// Fix broken types
RepoFinderResult.ref_to_timestamp type_arguments="OSTree.CollectionRef,uint64*"
RepoPruneOptions.reachable type_arguments="GLib.Variant,GLib.Variant" unowned=false
SysrootDeployTreeOpts
  .override_kernel_argv type="string[]?"
  .overlay_initrds type="string[]?"
checksum_file_at.stbuf type="Posix.Stat?"
CollectionRef.equal.*#parameter type="OSTree.CollectionRef"
CollectionRef.hash.ref type="OSTree.CollectionRef"
hash_object_name.a type="GLib.Variant"
Sysroot.write_deployments*.new_deployments type_arguments="unowned OSTree.Deployment"

// For convenience
MutableTree.remove.allow_noent default=true
RepoCommitTraverseIter.init*.flags default=RepoCommitTraverseFlags.NONE
RepoFinderAvahi.new.context default=null
RepoFinderMount.new.monitor default=null
SePolicy.fscreatecon_cleanup.unused default=null

// Missing floating references
object_name_serialize floating
GpgVerifyResult.get* floating

// Remove duplicates (TODO: Why are these happening in the first place?)
repo_finder_resolve_all_async skip
sign_get_all skip
sign_get_by_name skip

// Padding/unused fields
RepoTransactionStats.padding* skip
*Options.unused* skip
*Opts.unused* skip
RepoCommitTraverseIter.dummy* skip

// Skip broken CmdPrivateVTable stuff
cmd__private__ skip
CmdPrivateVTable skip

// Skip broken SignDummy and SignEd25519 classes
// They just implement the interface anyway
SignDummy skip
Sign.dummy_* skip
SignEd25519 skip
Sign.ed25519_* skip

// Remove unused and/or empty classes/methods/delegates
CollectionRefv skip
Bootloader* skip
GpgVerifier skip
KernelArgsEntry skip
LibarchiveInputStream skip
Lzma* skip
MutableTreeIter skip
RepoFileEnumerator* skip
RepoFinderResultv skip
RepoImportArchiveTranslatePathname skip
RollsumMatches skip
*Class skip
kernel_args_cleanup skip // Just calls kernel_args_free

// Fixup placement of some functions
gpg_error_quark parent="OSTree.GPGError" name="quark"
repo_commit_traverse_iter_cleanup parent="OSTree.RepoCommitTraverseIter" name="reset" symbol_type="method" instance_idx=0
Deployment.unlocked_state_to_string parent="OSTree.DeploymentUnlockedState" name="to_string" symbol_type="method" instance_idx=0

